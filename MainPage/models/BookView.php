<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_book_view".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $image_id
 * @property string $time
 */
class BookView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_book_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'image_id'], 'integer'],
            [['time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'image_id' => 'Image ID',
            'time' => 'Time',
        ];
    }
}
