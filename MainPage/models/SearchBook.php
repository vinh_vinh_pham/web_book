<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;

/**
 * SearchBook represents the model behind the search form about `app\models\Book`.
 */
class SearchBook extends Book
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'condition_id', 'manufacture_id', 'issuers_id', 'book_form_id', 'weight', 'page', 'year_id', 'width', 'height', 'type_id', 'creator_id', 'image_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'condition_id' => $this->condition_id,
            'manufacture_id' => $this->manufacture_id,
            'issuers_id' => $this->issuers_id,
            'book_form_id' => $this->book_form_id,
            'weight' => $this->weight,
            'page' => $this->page,
            'year_id' => $this->year_id,
            'width' => $this->width,
            'height' => $this->height,
            'type_id' => $this->type_id,
            'creator_id' => $this->creator_id,
            'image_id' => $this->image_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
