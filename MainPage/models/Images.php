<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_images".
 *
 * @property integer $id
 * @property string $name
 * @property string $ext
 * @property integer $width
 * @property integer $height
 * @property string $time
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['width', 'height'], 'integer'],
            [['time'], 'safe'],
            [['name', 'ext'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ext' => 'Ext',
            'width' => 'Width',
            'height' => 'Height',
            'time' => 'Time',
        ];
    }
}
