<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_price".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $sell
 * @property integer $market
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'sell', 'market'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'sell' => 'Sell',
            'market' => 'Market',
        ];
    }
}
