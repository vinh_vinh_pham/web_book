<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $displayname
 * @property string $password
 * @property string $email
 * @property integer $group_id
 * @property string $mobile
 * @property boolean $admin
 * @property string $createtime
 * @property boolean $active
 * @property boolean $sex
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    /**
     * @inheritdoc
     */
    public $confirm_password;
    public $new_password;
    public $old_password;

    public static function tableName() {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'new_password', 'displayname', 'confirm_password', 'email', 'old_password', 'sex','admin'], 'required', 'message' => '{attribute} không được bỏ trống'],
            [['username'], 'match', 'pattern' => '/^[a-zA-Z0-9\_]+$/u', 'message' => '{attribute} viết liền không dấu, cho phép ký tự \'_\''],
            [['new_password', 'confirm_password'], 'match', 'pattern' => '/^[A-Za-z0-9_!@#$%^&*()+=?.,null]+$/u', 'message' => '{attribute} không chứa dấu cách hoặc chữ có dấu'],
            [['group_id',], 'integer'],
            [['createtime'], 'safe'],
            [['username', 'password', 'displayname', 'email'], 'string', 'max' => 128],
            [['email'], 'email', 'message' => '{attribute} Không đúng địng dạng.'],
            [['mobile'], 'string', 'max' => 20],
            [['username', 'email'], 'unique', 'message' => '{attribute} Đã tồn tại.'],
            [['confirm_password'], 'compare', 'compareAttribute' => 'new_password', 'message' => 'Mật khẩu không trùng nhau'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Tên đăng nhập',
            'displayname' => 'Tên hiển thị',
            'password' => 'Password',
            'new_password' => 'Mật khẩu',
            'confirm_password' => 'Nhập lại mật khẩu',
            'old_password' => 'Mật khẩu cũ',
            'email' => 'Email',
            'group_id' => 'Nhóm',
            'mobile' => 'Điện thoại',
            'admin' => 'Admin',
            'createtime' => 'Createtime',
            'active' => 'Hoạt động',
            'sex'=>'Giới tính'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTblDevices() {
        return $this->hasMany(TblDevice::className(), ['creator_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTblFirmwares() {
        return $this->hasMany(TblFirmware::className(), ['creator_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTblVersions() {
        return $this->hasMany(TblVersion::className(), ['creator_id' => 'id']);
    }

    public function validatePassword($password) {
        return $password === $this->password;
    }

    public function confirmpasscode() {
        if ($this->password === $this->confirm_password)
            return TRUE;
        return FALSE;
    }

    public static function findByUsername($username) {
        return User::findOne(['username' => $username]);
    }

    public function getAuthKey() {
        return "cmd";
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {

        //throw new NotSupportedException("Not suport");
        return true;
    }

    public static function findIdentity($id) {
        return User::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException("Not suport");
    }

}
