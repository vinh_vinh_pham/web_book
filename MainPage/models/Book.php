<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_book".
 *
 * @property integer $id
 * @property string $name
 * @property integer $condition_id
 * @property integer $manufacture_id
 * @property integer $issuers_id
 * @property integer $book_form_id
 * @property integer $weight
 * @property integer $page
 * @property integer $year_id
 * @property integer $width
 * @property integer $height
 * @property integer $type_id
 * @property integer $creator_id
 * @property integer $image_id
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['condition_id', 'manufacture_id', 'issuers_id', 'book_form_id', 'weight', 'page', 'year_id', 'width', 'height', 'type_id', 'creator_id', 'image_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID sách',
            'name' => 'Tên sách',
            'condition_id' => 'Tình trạng',
            'manufacture_id' => 'Nhà xuất bản',
            'issuers_id' => 'Công ty phát hành',
            'book_form_id' => 'Hình thức',
            'weight' => 'Trọng lượng',
            'page' => 'Số trang',
            'year_id' => 'Năm sản xuất',
            'width' => 'Chiều dài',
            'height' => 'Chiều rộng',
            'type_id' => 'Loại sách',
            'creator_id' => 'Người thêm',
            'image_id' => 'Ảnh ID',
        ];
    }
}
