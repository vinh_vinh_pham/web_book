<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_book_type".
 *
 * @property integer $id
 * @property integer $product_list_id
 * @property integer $category_id
 */
class BookType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_book_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_list_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_list_id' => 'Danh mục sách',
            'category_id' => 'Loại sách',
        ];
    }
}
