<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HomeAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css', 'js/ddlevelsfiles/ddlevelsmenu-base.css', 'js/jcarousel/skin.css', 'js/autocomplete/jquery.autocomplete.css'
    ];
    public $js = [
        'js/ddlevelsfiles/ddlevelsmenu.js', 
        'js/autocomplete/jquery-1.7.min.js',
        'js/autocomplete/jquery.bgiframe.min.js',
        'js/autocomplete/jquery.ajaxQueue.js',
        'js/autocomplete/thickbox-compressed.js',
        'js/autocomplete/jquery.autocomplete.js',
        'js/scrolltopcontrol.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }

}
