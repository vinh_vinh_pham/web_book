<?php

namespace app\controllers;

use yii\base\Controller;
use app\controllers\ImageManipulator;

class ImageController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionUpload() {

        if (\Yii::$app->request->post()) {
            // resize and crop image 200x284
            // resize image
            $newNamePrefix = date("YmdHis");
            $manipulator = new ImageManipulator($_FILES['image']['tmp_name']);

            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

            // resizing to 200x200
            $newImage = $manipulator->resample(284, 284);
            // saving file to uploads folder
            $newImage->save('../uploads/' . $newNamePrefix . '.' . $ext);

//            // crop image
//            $newNamePrefix = time() . '_';
//            $manipulator = new ImageManipulator($_FILES['image']['tmp_name']);
//            $width = $manipulator->getWidth();
//            $height = $manipulator->getHeight();
//            $centreX = round($width / 2);
//            $centreY = round($height / 2);
//            // our dimensions will be 200x130
//            $x1 = $centreX - 100; // 200 / 2
//            $y1 = $centreY - 65; // 130 / 2
//
//            $x2 = $centreX + 100; // 200 / 2
//            $y2 = $centreY + 65; // 130 / 2
//            // center cropping to 200x130
//            $newImage = $manipulator->crop($x1, $y1, $x2, $y2);
//            // saving file to uploads folder
//            $manipulator->save('..//uploads/' . $newNamePrefix . $_FILES['image']['name']);
//            
            // save image database
            $image = new \app\models\Images();
            $image->name = $newNamePrefix . '';
            $image->ext = $ext;
            $image->width = 200;
            $image->height = 284;
            $image->save();
        }
        return $this->render('upload');
    }

}
