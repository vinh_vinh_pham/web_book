<?php

namespace app\controllers;

use Yii;
use app\models\BookView;
use app\models\SearchBookView;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookViewController implements the CRUD actions for BookView model.
 */
class BookViewController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BookView models.
     * @return mixed
     */
    public function actionIndex($id) {

        return $this->render('index', [
                    'id' => $id
        ]);
    }

    /**
     * Displays a single BookView model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BookView model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        $model = new BookView();
        if (Yii::$app->request->post()) {

            // save image server
            $count = count($_FILES['image']['name']);

            for ($i = 0; $i < $count; $i++) {
                $newNamePrefix = date("YmdHisa") + $i;
                $manipulator = new ImageManipulator($_FILES['image']['tmp_name'][$i]);

                $ext = pathinfo($_FILES['image']['name'][$i], PATHINFO_EXTENSION);
                $width = $manipulator->getWidth();
                $height = $manipulator->getHeight();

                $newImage = $manipulator;
                $newImage->save('../uploads/' . $newNamePrefix . '.' . $ext);

                // save database
                $image = new \app\models\Images();
                $image->name = $newNamePrefix . '';
                $image->ext = $ext;
                $image->width = $width;
                $image->height = $height;
                if ($image->save()) {
                    // save in bookview
                    $model = new BookView();
                    $model->book_id = $id;
                    $model->image_id = $image->id;
                    $model->save();
                }
            }
            return $this->render('index', [
                        'id' => $id,
            ]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing BookView model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BookView model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BookView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BookView the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BookView::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
