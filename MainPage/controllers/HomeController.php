<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class HomeController extends Controller {

    public $layout = "homelayout";

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionView() {
        $id = $_GET['book_id'];
        $model = \app\models\Book::findOne($id);
        return $this->render('view', ['book' => $model]);
    }

    public function actionType() {

        isset($_GET['pr_id']) ? $pr_id = $_GET['pr_id'] : $pr_id = 0;
        isset($_GET['cate_id']) ? $cate_id = $_GET['cate_id'] : $cate_id = 0;

        return $this->render('type', ['pr_id' => $pr_id, 'cate_id' => $cate_id]);
    }

    public function actionDetail() {
        return $this->render('detail');
    }

}
