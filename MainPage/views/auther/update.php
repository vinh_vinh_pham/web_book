<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auther */

$this->title = 'Chình sửa tác giả: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tác giả', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Chình sửa';
?>
<div class="auther-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
