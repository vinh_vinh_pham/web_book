<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>
<div class="user-view">

    <h1><?= Html::encode('Xin chào: ' . $model->username) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-striped'],
        'attributes' => [
            'username',
            'password',
            'displayname',
            'email:email',
            'mobile',
        ],
    ])
    ?>

    <p>
        <?= Html::a('Sửa thông tin', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
