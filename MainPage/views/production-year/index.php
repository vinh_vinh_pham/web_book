<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchProductionYear */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Năm hiện có';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="production-year-index">
    <p>
        <?= Html::a('Thêm một năm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'STT'],

            'year',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
