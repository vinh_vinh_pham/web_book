<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBook */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tất cả sách';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    td img{
        width: 20px;
        height: 25px;
    }
</style>
<div class="book-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Chọn danh mục sách
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <?php
            $prList = \app\models\ProductList::find()->all();
            foreach ($prList as $key => $value) {
                echo '<li>' . Html::a($value['name'], ['index']) . '</li>';
            }
            ?>
        </ul>

        <?= Html::a('Thêm sách', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'STT'],
            [
                'format' => 'image',
                'value' => function ($model) {
                    $image = app\models\Images::findOne($model->image_id) ? "http://localhost/book/uploads/" . (app\models\Images::findOne($model->image_id)->name . '.' . app\models\Images::findOne($model->image_id)->ext) : '';
                    return $image;
                },
            ],
            'name',
            [
                'attribute' => 'Tình trạng',
                'format' => 'raw',
                'value' => function ($model) {
                    return (app\models\Condition::findOne($model->condition_id)->name);
                },
            ],
            [
                'attribute' => 'Nhà sản xuất',
                'format' => 'raw',
                'value' => function ($model) {
                    return app\models\Manufacturer::findOne($model->manufacture_id) ? (app\models\Manufacturer::findOne($model->manufacture_id)->name) : 'Không có';
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
