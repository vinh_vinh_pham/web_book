<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BookForm */

$this->title = 'Sửa hình thức: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Hình thức sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="book-form-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
