<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductList */

$this->title = 'Thêm danh mục sách';
$this->params['breadcrumbs'][] = ['label' => 'Danh mục sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
