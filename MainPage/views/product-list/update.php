<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductList */

$this->title = 'Sửa danh mục: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh mục sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="product-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
