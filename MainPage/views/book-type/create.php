<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BookType */

$this->title = 'Thêm loại sách mới';
$this->params['breadcrumbs'][] = ['label' => 'Loại sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
