<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Issuers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'CÔng ty phát hành', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issuers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-striped'],
        'attributes' => [
            'name',
            'address',
            'phone',
        ],
    ])
    ?>

</div>
