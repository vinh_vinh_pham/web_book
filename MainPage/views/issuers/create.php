<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Issuers */

$this->title = 'Thêm';
$this->params['breadcrumbs'][] = ['label' => 'Công ty phát hành', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issuers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
