<?php
$this->title = 'Tải hình ảnh';
$this->params['breadcrumbs'][] = $this->title;
$action = \Yii::$app->getUrlManager()->createUrl('image/upload');
?>

<?php
$form = yii\widgets\ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'form-inline',
                'enctype' => 'multipart/form-data'
            ],
        ]);
?>
<div class="form-group">
<?= yii\helpers\Html::fileInput("image", '', ['class' => "btn btn-info"]) ?>
</div>
<div class="form-group">
<?= yii\helpers\Html::submitButton('Tải lên', ['class' => "btn btn-success"]) ?>
</div>
    <?php
    yii\widgets\ActiveForm::end();
    ?>