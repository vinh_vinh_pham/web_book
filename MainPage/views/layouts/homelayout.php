<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\HomeAsset;

/* @var $this \yii\web\View */
/* @var $content string */

HomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <style>
        .logo{
            width: 219px;
            height: 80px;
        }
    </style>
    <body>
        <?php $this->beginBody() ?>
        <div id="header">
            <div class="header-inner">
                <div class="nav-top">

                    <div class="user-info">

                        <div id="HeaderControl1_pnlLogin" style="float: right">

                            <a href="#">Đăng nhập</a> <span class="sep">|</span> <a href="#">Đăng
                                ký</a> <span class="sep">|</span> <a href="#">Quên mật khẩu</a>

                        </div>
                    </div>

                    <div style="float: right">
                        <a href="#">Trang chủ</a> <span class="sep">|</span>
                        <a href="#">Yêu cầu tìm sách</a> <span class="sep">|</span>
                        <a href="#">Tin tức</a> <span class="sep">|</span>

                        <span class="sep">|</span>
                    </div>
                </div>
                <div class="clear">
                </div>
                <a href="<?= \Yii::$app->getUrlManager()->createUrl(['home/index']); ?>">
                    <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xft1/v/t1.0-1/p160x160/12500_261664400699275_1857583687014073804_n.png?oh=f28dbd603aea970bfa24b1f2591ce661&oe=56121BAC&__gda__=1444675184_68457df626822f1b33fb4f2271086fae" class="logo" />
                </a>
                <div class="banner-gioithieu">

                    <div class="trahang">
                        <div class="trahang-icon">
                        </div>
                        <a href="#">Tải đề thi thử đại học</a>
                    </div>
                    <div class="giaohang">
                        <div class="giaohang-icon">
                        </div>
                        Giao hàng toàn quốc 
                    </div>

                    <div class="thanhtoan">
                        <div class="thanhtoan-icon">
                        </div>
                        Thanh toán khi nhận hàng
                    </div>
                    <div class="hotline">
                        <div class="hotline-icon">
                        </div>
                        Hotline
                        <br />
                        <div class="hotline-red">
                            </br>
                            Đình Anh : 0982 783 325 
                            </br>
                            Khắc Quỹ: 0972 654 452
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="main-bar">

                    <div class="category">
                        <div id="HeaderControl1_MenuMainControl1_pnlHome">

                            <div style="width: 220px; height: 400px">
                                <ul class="category-title">
                                    <li>Danh mục sản phẩm</li>
                                </ul>
                                <div class="clear">
                                </div>
                                <div class="body markermenu" id="ddsidemenubar">
                                    <ul>
                                        <?php
                                        $prl = app\models\ProductList::find()->all();
                                        foreach ($prl as $key => $value) {
                                            echo '<li>' . (Html::a($value['name'], ['type','pr_id' => $value['id']], ['rel' => 'prl_' . $value['id']])) . '</li>';
                                        }
                                        ?>
                                    </ul>
                                    <script type="text/javascript">
                                        ddlevelsmenu.setup("ddsidemenubar", "sidebar")
                                    </script>

                                    <?php foreach ($prl as $key => $value1) { ?>
                                        <ul id="<?= 'prl_' . $value1['id'] ?>" class="ddsubmenustyle blackwhite">
                                            <?php
                                            $cate = app\models\Category::find(['product_list_id' => $value['id']])
                                                            ->innerJoin("tbl_book_type as tp", 'tp.category_id = tbl_category.id')
                                                            ->andWhere(['tp.product_list_id' => $value1['id']])->all();
                                            foreach ($cate as $key => $value) {
                                                echo '<li>' . (Html::a($value['name'], ['type', 'pr_id' => $value1['id'], 'cate_id' => $value['id']], ['rel' => 'prl_' . $value1['id'],])) . '</li>';
                                            }
                                            ?>
                                        </ul>
                                    <?php } ?>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="HeaderControl1_SearchBoxControl1_Panel1" class="search-box">


                        <input name="HeaderControl1$SearchBoxControl1$txtSearch" type="text" value="Tên sách hoặc tác giả" id="HeaderControl1_SearchBoxControl1_txtSearch" class="inputsearch ac_input" autocomplete="off" />
                        <input type="image" name="HeaderControl1$SearchBoxControl1$btnSearch" id="HeaderControl1_SearchBoxControl1_btnSearch" class="searchbuton" src="http://localhost/book_website/web/images/blank.png" />

                    </div>

                    <div class="shoppingcart">
                        <a href="Shopping-cart.aspx.html">
                            <img src="http://localhost/book_website/web/images/shoppingcart.png" alt="Shopping Cart" /></a>
                    </div>

                </div>
            </div>
        </div>

        <?= $content; ?>

        <div class="clear">
        </div>


        <div class="footer-menu">
            <div class="title">

                <div class="item">
                    <span>
                        Trợ giúp</span>
                </div>

                <div class="item">
                    <span>
                        Đăng nhập/Đăng ký</span>
                </div>

                <div class="item">
                    <span>
                        Về chúng tôi</span>
                </div>

            </div>
            <div class="clear">
            </div>
            <div class="body">

                <ul>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_0_hplTitle_0" href="#" target="_blank">Câu hỏi thường gặp</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_0_hplTitle_1" href="#">Hướng dẫn mua hàng</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_0_hplTitle_2" href="#">Vận chuyển & Cước phí</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_0_hplTitle_3" href="#">Góp ý và khiếu nại</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_0_hplTitle_4" href="#">Liên hệ hỗ trợ</a></li>

                </ul>

                <ul>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_1_hplTitle_0" href="#">Đăng nhập/Đăng ký</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_1_hplTitle_1" href="#">Thay đổi thông tin</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_1_hplTitle_2" href="#">Lấy lại mật khẩu</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_1_hplTitle_3" href="#">Giỏ hàng</a></li>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_1_hplTitle_4" href="#">Chính Sách Bảo Mật</a></li>

                </ul>

                <ul>

                    <li>
                        <a id="FooterControl1_rptParent_rptChild_2_hplTitle_0" href="#">Giới thiệu nhasachhoahoctro.com</a></li>

                </ul>

            </div>
        </div>

        <div class="clear">
        </div>
        <div class="footer">
            <div>

            </div>
        </div>


        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
