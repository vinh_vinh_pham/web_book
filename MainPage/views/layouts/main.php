<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <style>
        /* navbar */
        .navbar-default {
            background-color: #006699;
            border-color: #006699;
        }
        /* title */
        .navbar-default .navbar-brand {
            color: #FFF;
        }
        .navbar-default .navbar-brand:hover,
        .navbar-default .navbar-brand:focus {
            color: #3a9bf4;
        }
        /* link */
        .navbar-default .navbar-nav > li > a {
            color: #FFF;
        }
        .navbar-default .navbar-nav > li > a:hover,
        .navbar-default .navbar-nav > li > a:focus {
            color: #3a9bf4;
        }
        .navbar-default .navbar-nav > .active > a, 
        .navbar-default .navbar-nav > .active > a:hover, 
        .navbar-default .navbar-nav > .active > a:focus {
            color: #555;
            background-color: #daf1ff;
        }
        .navbar-default .navbar-nav > .open > a, 
        .navbar-default .navbar-nav > .open > a:hover, 
        .navbar-default .navbar-nav > .open > a:focus {
            color: #555;
            background-color: #D5D5D5;
        }
        /* caret */
        .navbar-default .navbar-nav > .dropdown > a .caret {
            border-top-color: #777;
            border-bottom-color: #777;
        }
        .navbar-default .navbar-nav > .dropdown > a:hover .caret,
        .navbar-default .navbar-nav > .dropdown > a:focus .caret {
            border-top-color: #333;
            border-bottom-color: #333;
        }
        .navbar-default .navbar-nav > .open > a .caret, 
        .navbar-default .navbar-nav > .open > a:hover .caret, 
        .navbar-default .navbar-nav > .open > a:focus .caret {
            border-top-color: #555;
            border-bottom-color: #555;
        }
        /* mobile version */
        .navbar-default .navbar-toggle {
            border-color: #DDD;
        }
        .navbar-default .navbar-toggle:hover,
        .navbar-default .navbar-toggle:focus {
            background-color: #DDD;
        }
        .navbar-default .navbar-toggle .icon-bar {
            background-color: #CCC;
        }
        @media (max-width: 767px) {
            .navbar-default .navbar-nav .open .dropdown-menu > li > a {
                color: #777;
            }
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
                color: #333;
            }
        }

        .footer{
            background: #006699;
            color: #FFF;
        }
    </style>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Quản trị sách',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Sách', 'url' => ['/book/index']],
                    ['label' => 'Tài khoản',
                       'url' => ['/user/view', 'id' => Yii::$app->user ? Yii::$app->user->id : '']],
                    Yii::$app->user->isGuest ?
                            ['label' => 'Đăng nhập', 'url' => ['/site/login']] :
                            ['label' => 'Thoát (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
