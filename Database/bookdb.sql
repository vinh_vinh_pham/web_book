-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: book_web
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_auther`
--

DROP TABLE IF EXISTS `tbl_auther`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_auther` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_auther`
--

LOCK TABLES `tbl_auther` WRITE;
/*!40000 ALTER TABLE `tbl_auther` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_auther` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book`
--

DROP TABLE IF EXISTS `tbl_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `manufacture_id` int(11) DEFAULT NULL,
  `issuers_id` int(11) DEFAULT NULL,
  `book_form_id` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `page` int(11) DEFAULT NULL,
  `year_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book`
--

LOCK TABLES `tbl_book` WRITE;
/*!40000 ALTER TABLE `tbl_book` DISABLE KEYS */;
INSERT INTO `tbl_book` VALUES (6,'Hóa học',1,1,1,1,NULL,NULL,12,NULL,NULL,26,NULL,32),(7,'Ngữ văn',2,1,1,1,200,500,12,100,200,10,NULL,34),(8,'Vật lý',1,1,1,1,NULL,NULL,12,NULL,NULL,21,NULL,36),(9,'Sinh học',1,1,NULL,1,NULL,NULL,1,NULL,NULL,1,NULL,35);
/*!40000 ALTER TABLE `tbl_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book_auther`
--

DROP TABLE IF EXISTS `tbl_book_auther`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book_auther` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `auther_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book_auther`
--

LOCK TABLES `tbl_book_auther` WRITE;
/*!40000 ALTER TABLE `tbl_book_auther` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_book_auther` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book_form`
--

DROP TABLE IF EXISTS `tbl_book_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book_form`
--

LOCK TABLES `tbl_book_form` WRITE;
/*!40000 ALTER TABLE `tbl_book_form` DISABLE KEYS */;
INSERT INTO `tbl_book_form` VALUES (1,'Bìa cứng - In màu'),(2,'Bìa cứng - Không màu'),(3,'Bìa mềm');
/*!40000 ALTER TABLE `tbl_book_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book_type`
--

DROP TABLE IF EXISTS `tbl_book_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_list_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book_type`
--

LOCK TABLES `tbl_book_type` WRITE;
/*!40000 ALTER TABLE `tbl_book_type` DISABLE KEYS */;
INSERT INTO `tbl_book_type` VALUES (1,1,2),(2,1,3),(3,1,4),(4,1,5),(5,1,6),(6,1,7),(7,1,8),(8,1,9),(9,1,2),(10,2,2),(11,2,3),(12,2,4),(13,2,5),(14,2,6),(15,2,7),(16,2,8),(17,2,9),(18,3,2),(19,3,3),(20,3,4),(21,3,5),(22,3,6),(23,3,7),(24,3,8),(25,3,9),(26,5,2),(27,5,7),(28,5,6),(29,6,10),(30,6,11),(31,6,13),(32,8,2),(33,8,3),(34,8,4),(35,8,5),(36,8,6),(37,8,7),(38,8,8),(39,8,9);
/*!40000 ALTER TABLE `tbl_book_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book_view`
--

DROP TABLE IF EXISTS `tbl_book_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book_view`
--

LOCK TABLES `tbl_book_view` WRITE;
/*!40000 ALTER TABLE `tbl_book_view` DISABLE KEYS */;
INSERT INTO `tbl_book_view` VALUES (14,6,29,NULL),(15,6,30,NULL),(16,6,31,NULL),(17,11,38,NULL),(18,11,39,NULL),(19,11,40,NULL),(20,11,41,NULL),(21,11,42,NULL),(22,11,43,NULL),(23,11,44,NULL),(24,11,45,NULL);
/*!40000 ALTER TABLE `tbl_book_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category`
--

LOCK TABLES `tbl_category` WRITE;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` VALUES (2,'MÔN TOÁN'),(3,'MÔN VẬT LÝ'),(4,'MÔN HÓA HỌC'),(5,'MÔN SINH HỌC'),(6,'MÔN TIẾNG ANH'),(7,'MÔN NGỮ VĂN'),(8,'MÔN LỊCH SỬ'),(9,'MÔN ĐỊA LÝ'),(10,'HẠT GIỐNG TÂM HỒN'),(11,'TRUYỆN - TIỂU THUYẾT'),(13,'KỸ NĂNG MỀM');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_condition`
--

DROP TABLE IF EXISTS `tbl_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_condition`
--

LOCK TABLES `tbl_condition` WRITE;
/*!40000 ALTER TABLE `tbl_condition` DISABLE KEYS */;
INSERT INTO `tbl_condition` VALUES (1,'Còn hàng'),(2,'Hết hàng'),(3,'Sắp có');
/*!40000 ALTER TABLE `tbl_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_images`
--

DROP TABLE IF EXISTS `tbl_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_images`
--

LOCK TABLES `tbl_images` WRITE;
/*!40000 ALTER TABLE `tbl_images` DISABLE KEYS */;
INSERT INTO `tbl_images` VALUES (29,'20150705105934','jpg',200,248,'2015-07-05 15:59:34'),(30,'20150705105935','jpg',200,284,'2015-07-05 15:59:34'),(31,'20150705110337','jpg',200,284,'2015-07-05 16:03:37'),(32,'20150705120211','jpg',200,284,'2015-07-05 17:02:11'),(33,'20150705120243','jpg',200,284,'2015-07-05 17:02:43'),(34,'20150705122843','jpg',200,284,'2015-07-05 17:28:43'),(35,'20150705122918','jpg',200,284,'2015-07-05 17:29:18'),(36,'20150705141433','jpg',200,284,'2015-07-05 19:14:33'),(37,'20150709195923','jpg',200,284,'2015-07-10 00:59:24'),(38,'20150709200000','jpg',1024,768,'2015-07-10 01:00:00'),(39,'20150709200001','jpg',1024,768,'2015-07-10 01:00:00'),(40,'20150709200002','jpg',1024,768,'2015-07-10 01:00:01'),(41,'20150709200004','jpg',1024,768,'2015-07-10 01:00:01'),(42,'20150709200005','jpg',1024,768,'2015-07-10 01:00:01'),(43,'20150709200006','jpg',1024,768,'2015-07-10 01:00:01'),(44,'20150709200007','jpg',1024,768,'2015-07-10 01:00:01'),(45,'20150709200009','jpg',1024,768,'2015-07-10 01:00:02'),(46,'20150711185804','png',200,284,'2015-07-11 23:58:05');
/*!40000 ALTER TABLE `tbl_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_issuers`
--

DROP TABLE IF EXISTS `tbl_issuers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_issuers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_issuers`
--

LOCK TABLES `tbl_issuers` WRITE;
/*!40000 ALTER TABLE `tbl_issuers` DISABLE KEYS */;
INSERT INTO `tbl_issuers` VALUES (1,'NHÀ SÁCH HOA H?C TRÒ','NG?C T?O - PHÚC TH? - HÀ N?I','0982783325');
/*!40000 ALTER TABLE `tbl_issuers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_manufacturer`
--

DROP TABLE IF EXISTS `tbl_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_manufacturer`
--

LOCK TABLES `tbl_manufacturer` WRITE;
/*!40000 ALTER TABLE `tbl_manufacturer` DISABLE KEYS */;
INSERT INTO `tbl_manufacturer` VALUES (1,'Bộ giáo dục và đào tạo','',''),(2,'LoveBook','Địa chỉ: 101 Nguyễn Ngọc Nại, Thanh Xuân, Hà Nội ','(04) 6686 0849');
/*!40000 ALTER TABLE `tbl_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_price`
--

DROP TABLE IF EXISTS `tbl_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `sell` int(11) DEFAULT NULL,
  `market` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_price`
--

LOCK TABLES `tbl_price` WRITE;
/*!40000 ALTER TABLE `tbl_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_list`
--

DROP TABLE IF EXISTS `tbl_product_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_list`
--

LOCK TABLES `tbl_product_list` WRITE;
/*!40000 ALTER TABLE `tbl_product_list` DISABLE KEYS */;
INSERT INTO `tbl_product_list` VALUES (1,'SÁCH LUYỆN THI ĐẠI HỌC'),(2,'LỚP 11'),(3,'LỚP 10'),(5,'SÁCH THI VÀO LỚP 10'),(6,'CÁC LOẠI SÁCH KHÁC'),(7,'HỌC TIẾNG ANH'),(8,'CHIA SẺ TÀI LIỆU');
/*!40000 ALTER TABLE `tbl_product_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_production_year`
--

DROP TABLE IF EXISTS `tbl_production_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_production_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_production_year`
--

LOCK TABLES `tbl_production_year` WRITE;
/*!40000 ALTER TABLE `tbl_production_year` DISABLE KEYS */;
INSERT INTO `tbl_production_year` VALUES (12,2013),(13,2014),(14,2015),(15,2016),(16,2017),(17,2018),(18,2019),(19,2020);
/*!40000 ALTER TABLE `tbl_production_year` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `displayname` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `isAdmin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,1,'admin','1234','admin','admin@gmail.com','12345678',NULL,NULL,1,1);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-12 19:45:44
