﻿        $().ready(function () {

            function log(event, data, formatted) {
                $("<li>").html(!data ? "No match!" : "Selected: " + formatted).appendTo("#result");
            }

            function formatItem(row) {
                return row[0] + " (<strong>id: " + row[1] + "</strong>)";
            }
            function formatResult(row) {
                return row[0].replace(/(<.+?>)/gi, '');
            }


            $(".inputsearch").autocomplete("/Autocomplete.aspx", {
                width: 455,
                selectFirst: false,
                mustMatch: false,
                autoFill: false,
                matchContains: false,
                minChars: 0
            });

//            $(".inputsearch").autocomplete("/Hanhtrangvaodaihoc_web/Autocomplete.aspx", {
//                width: 350,
//                max: 10,
//                highlight: false,
//                scroll: true,
//                scrollHeight: 300,
//                formatItem: function (data, i, n, value) {
//                    return "<img width='50' height='50' style='float:left; margin:5px' src='/upload/images_products/small/" + value.split(".")[1] + "." + value.split(".")[2] + "'/> " + value.split(".")[0];
//                },
//                formatResult: function (data, value) {
//                    return value.split(".")[0];
//                }
//            });
        });



        $(document).ready(function () {
            try {
                doMakeFixed("fixed_ads", { bottom: '#foot-content' });
            }
            catch (e) { }
        });       








/*-----jquery selectbox---*/
jQuery.fn.extend({
    selectbox: function (options) {
        return this.each(function () {
            new jQuery.SelectBox(this, options);
        });
    }
});

jQuery.SelectBox = function (selectobj, options) {

    var opt = options || {};
    opt.inputClass = opt.inputClass || "selectbox";
    opt.containerClass = opt.containerClass || "selectbox-wrapper";
    opt.hoverClass = opt.hoverClass || "selected";
    opt.debug = opt.debug || false;

    var elm_id = selectobj.id;
    var active = -1;
    var inFocus = false;
    var hasfocus = 0;
    //jquery object for select element
    var $select = $(selectobj);
    // jquery container object
    var $container = setupContainer(opt);
    //jquery input object 
    var $input = setupInput(opt);
    // hide select and append newly created elements
    $select.hide().before($input).before($container);

    init();

    $input
	.click(function () {
	    if (!inFocus) {
	        $container.toggle();
	    }
	})
	.focus(function () {
	    if ($container.not(':visible')) {
	        inFocus = true;
	        $container.show();
	    }
	})
	.keydown(function (event) {
	    switch (event.keyCode) {
	        case 38: // up
	            event.preventDefault();
	            moveSelect(-1);
	            break;
	        case 40: // down
	            event.preventDefault();
	            moveSelect(1);
	            break;
	        //case 9:  // tab       
	        case 13: // return
	            event.preventDefault(); // seems not working in mac !
	            setCurrent();
	            hideMe();
	            break;
	    }
	})
	.blur(function () {
	    if ($container.is(':visible') && hasfocus > 0) {
	        if (opt.debug) console.log('container visible and has focus')
	    } else {
	        hideMe();
	    }
	});


    function hideMe() {
        hasfocus = 0;
        $container.hide();
    }

    function init() {
        $container.append(getSelectOptions()).hide();
        var width = $input.width() + 50
        $container.width(width);
    }

    function setupContainer(options) {
        var container = document.createElement("div");
        $container = $(container);
        $container.attr('id', elm_id + '_container');
        $container.addClass(options.containerClass);

        return $container;
    }

    function setupInput(options) {
        var input = document.createElement("input");
        var $input = $(input);
        $input.attr("id", elm_id + "_input");
        $input.attr("type", "text");
        $input.addClass(options.inputClass);
        $input.attr("autocomplete", "off");
        $input.attr("readonly", "readonly");
        $input.attr("tabIndex", $select.attr("tabindex")); // "I" capital is important for ie

        return $input;
    }

    function moveSelect(step) {
        var lis = $("li", $container);
        if (!lis) return;

        active += step;

        if (active < 0) {
            active = 0;
        } else if (active >= lis.size()) {
            active = lis.size() - 1;
        }

        lis.removeClass(opt.hoverClass);

        $(lis[active]).addClass(opt.hoverClass);
    }

    function setCurrent() {
        var li = $("li." + opt.hoverClass, $container).get(0);
        var el = li.id
        $select.val(el);
        $input.val($(li).html());
        return true;
    }

    // select value
    function getCurrentSelected() {
        return $select.val();
    }

    // input value
    function getCurrentValue() {
        return $input.val();
    }

    function getSelectOptions() {
        var select_options = new Array();
        var ul = document.createElement('ul');
        $select.children('option').each(function () {
            var li = document.createElement('li');
            li.setAttribute('id', $(this).val());
            li.innerHTML = $(this).html();
            if ($(this).is(':selected')) {
                $input.val($(this).html());
                $(li).addClass(opt.hoverClass);

            }
            ul.appendChild(li);
            $(li)
			.mouseover(function (event) {
			    hasfocus = 1;
			    if (opt.debug) console.log('out on : ' + this.id);
			    jQuery(event.target, $container).addClass(opt.hoverClass);
			})
			.mouseout(function (event) {
			    hasfocus = -1;
			    if (opt.debug) console.log('out on : ' + this.id);
			    jQuery(event.target, $container).removeClass(opt.hoverClass);
			})
			.click(function (event) {
			    if (opt.debug) console.log('click on :' + this.id);
			    $(this).addClass(opt.hoverClass);
			    setCurrent();
			    hideMe();
			    // them boi Tuan Son
			    //insertParam("exchangerate", this.id);
			    setCookie("ExchangeRate", this.id, "30");
			    location.reload(true);
			});
        });
        return ul;
    }

};
/*----*/

function insertParam(key, value) {
    key = escape(key); value = escape(value);

    var kvp = document.location.search.substr(1).split('&');

    var i = kvp.length; var x; while (i--) {
        x = kvp[i].split('=');

        if (x[0] == key) {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if (i < 0) { kvp[kvp.length] = [key, value].join('='); }

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&');
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}


/////////////////////////////////// Menu

/*

$(function () {
    var navs = $(".menu-left a");
    var cur_nav = $(".menu-left a.active").get(0);
    navs.mouseover(function () {
        var _self = this;
        navs.each(function (i) {
            $(this).attr('class', this == _self ? 'active' : 'hover');

            if(this == _self)
            $(this).attr('style',"");
        });
    });
    navs.mouseout(function () {
        navs.each(function (i) {
            $(this).attr('class', this == cur_nav ? 'active' : 'hover');
        });
    });
})

*/