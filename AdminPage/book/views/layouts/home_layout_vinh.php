<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\HomeAsset;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
HomeAsset::register($this);

$web_url = Yii::getAlias('@web') . "/";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <header>
            <div class="div-headercontainer">
                <div class="div-link-top nav">
                    <a href="#">Trang chủ</a>
                    <span class="sep">|</span>
                    <a href="#">Yêu cầu tìm sách</a>                    
                    <span class="sep">|</span>
                    <a href="#">Đăng ký</a>
                    <span class="sep">|</span>
                    <a href="#">Đăng nhập</a>

                </div>

                <div class="clear"></div>
                <div class="big-logo">
                    <a href="#">
                        <img src="<?= Yii::getAlias('@web') ?>/img/nha-sach-hoa-hoc-tro.png" />
                    </a>
                </div>

                <div class="baner-introduction">
                    <div class="giao-hang">
                        <span class="introduction-text">Free ship toàn quốc</span>
                    </div>

                    <div class="thanh-toan">                        
                        <span class="introduction-text">Thanh toán sau nhận hang</span>
                    </div>
                    <div class="hot-line">                        
                        <span class="introduction-text normar-text">Đình Anh : 0982 783 325 <br/>Khắc Quỹ: 0972 654 452</span>                        
                    </div>
                </div>
                <div class="clear"></div>
                <div class="control-bar">                 

                    <?php
                    echo $this->render("@app/views/header-com/menu-book");
                    ?>
                    <div class="Search-bar form-inline">      
                        <div class="div-search-container form-control" >
                            <input type="text" id="txt-search" autofocus="" placeholder="Tìm theo tên sách, tác giả..." class="" />
                        </div>                        
                        <button type="button" id="btn-search" class="btn btn-primary form-control">Tìm kiếm</button>
                    </div>
                    <div class="shop-cart">
                        <a href="#">
                            <img src="<?php echo $web_url; ?>img/" />
                            <span id="shop-cart-text">8 Sản phẩm</span>
                        </a>
                    </div>
                    <div class="clear"></div>
                    <div class="AD">
                        <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
                        <div id="wowslider-container0">
                            <div class="ws_images"><ul>
                                    <li><img src="<?= Yii::getAlias('@web') ?>/data0/images/koala.jpg" alt="Koala" title="Koala" id="wows0_0"/></li>
                                    <li><img src="<?= Yii::getAlias('@web') ?>/data0/images/lighthouse.jpg" alt="Lighthouse" title="Lighthouse" id="wows0_1"/></li>
                                    <li><img src="<?= Yii::getAlias('@web') ?>/data0/images/penguins.jpg" alt="html slideshow" title="Penguins" id="wows0_2"/></a></li>
                                    <li><img src="<?= Yii::getAlias('@web') ?>/data0/images/tulips.jpg" alt="Tulips" title="Tulips" id="wows0_3"/></li>
                                </ul></div>
                            <div class="ws_bullets"><div>
                                    <a href="#" title="Koala"><span><img src="<?= Yii::getAlias('@web') ?>/data0/tooltips/koala.jpg" alt="Koala"/>1</span></a>
                                    <a href="#" title="Lighthouse"><span><img src="<?= Yii::getAlias('@web') ?>/data0/tooltips/lighthouse.jpg" alt="Lighthouse"/>2</span></a>
                                    <a href="#" title="Penguins"><span><img src="<?= Yii::getAlias('@web') ?>/data0/tooltips/penguins.jpg" alt="Penguins"/>3</span></a>
                                    <a href="#" title="Tulips"><span><img src="<?= Yii::getAlias('@web') ?>/data0/tooltips/tulips.jpg" alt="Tulips"/>4</span></a>
                                </div>
                            </div>

                            <div class="ws_shadow"></div>
                        </div>	
                        <script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/engine0/wowslider.js"></script>
                        <script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/engine0/script.js"></script>
                        <!-- End WOWSlider.com BODY section -->
                    </div>

                    <div class="facebook-cn">
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="170" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="div-register-mail-feed panel panel-primary">  
                        <div class="panel-body" style="">
                            <div class="row" style="">
                                <div class="col-lg-6 lable-register-mail" style="display: table-cell; vertical-align: middle;">
                                    <span class="text-uppercase">Đăng ký ngay để nhận thông tin mới nhất</span>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Nhập email...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="button">Đăng ký</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

            </div>
        </header>
        <div class="clear"></div>
        <div class="boddy">
            <div class="product">

                <div class="product-head">
                    <div class="category-name"><span class="text-uppercase">Sách lớp 11</span></div>
                    <div class="category-sub">
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="product-list connected-carousels">

                    <div class="navigation">
                        <a href="#" class="prev prev-navigation">&lsaquo;</a>
                        <a href="#" class="next next-navigation">&rsaquo;</a>
                        <div class="carousel carousel-navigation">
                            <ul>
                                <li>
                                    <div class="book">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">Những Viên Kim Cương Trong Hoá Học</div>
                                        <div class="book-price-1">30.000 USD</div>
                                        <div class="book-price-2">20.000 USD</div>
                                        <div class="book-price-3">10.000 USD</div>
                                    </div>
                                </li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name"><span class="text-center">asda 1</span></div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <div class="product">

                <div class="product-head">
                    <div class="category-name"><span class="text-uppercase">Sách lớp 11</span></div>
                    <div class="category-sub">
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="product-list connected-carousels">

                    <div class="navigation">
                        <a href="#" class="prev prev-navigation">&lsaquo;</a>
                        <a href="#" class="next next-navigation">&rsaquo;</a>
                        <div class="carousel carousel-navigation">
                            <ul>
                                <li>
                                    <div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">Những Viên Kim Cương Trong Hoá Học</div>
                                        <div class="book-price-1">30.000 USD</div>
                                        <div class="book-price-2">20.000 USD</div>
                                        <div class="book-price-3">10.000 USD</div>
                                    </div>
                                </li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name"><span class="text-center">asda 1</span></div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <div class="product">

                <div class="product-head">
                    <div class="category-name"><span class="text-uppercase">Sách lớp 11</span></div>
                    <div class="category-sub">
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>|
                        <a href="#">Địa Lý</a>                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="product-list connected-carousels">

                    <div class="navigation">
                        <a href="#" class="prev prev-navigation">&lsaquo;</a>
                        <a href="#" class="next next-navigation">&rsaquo;</a>
                        <div class="carousel carousel-navigation">
                            <ul>
                                <li>
                                    <div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">Những Viên Kim Cương Trong Hoá Học</div>
                                        <div class="book-price-1">30.000 USD</div>
                                        <div class="book-price-2">20.000 USD</div>
                                        <div class="book-price-3">10.000 USD</div>
                                    </div>
                                </li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name"><span class="text-center">asda 1</span></div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                                <li><div class="book">
                                        <img src="<?= $web_url ?>img/Nhung-Vien-Kim-Cuong-Trong-Hoa-Hoc.jpg" alt="">
                                        <div class="book-name">SÁch 1</div>
                                        <div class="book-price-1">30000</div>
                                        <div class="book-price-2">20000</div>
                                        <div class="book-price-3">10000</div>
                                    </div></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
