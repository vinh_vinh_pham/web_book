<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBookType */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loại sách';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-type-index">
    <p>
        <?= Html::a('Thêm loại sách', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'STT'],
            [
                'header' => 'Danh mục sách',
                'format' => 'raw',
                'value' => function ($model) {
                    return \app\models\ProductList::findOne($model->product_list_id)->name;
                },
            ],
            [
                'header' => 'Thể loại',
                'format' => 'raw',
                'value' => function ($model) {
                    return \app\models\Category::findOne($model->category_id)->name;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
