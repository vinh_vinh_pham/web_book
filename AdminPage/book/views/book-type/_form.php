<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BookType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-type-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php
    $condition = \app\models\ProductList::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('product-list/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'product_list_id', ['template' => '<a href="' . $action . '" target="_blank">Danh mục sách</a>{input}'])->dropDownList($listData) ?>
    </div>
    
    <?php
    $condition = \app\models\Category::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('category/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'category_id', ['template' => '<a href="' . $action . '" target="_blank">Thể loại</a>{input}'])->dropDownList($listData) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Lưu', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
