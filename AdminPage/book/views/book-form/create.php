<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BookForm */

$this->title = 'Thêm hình thức';
$this->params['breadcrumbs'][] = ['label' => 'Hình thức sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
