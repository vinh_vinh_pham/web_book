<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Auther */

$this->title = 'Thêm tác giả';
$this->params['breadcrumbs'][] = ['label' => 'Tác giả', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auther-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
