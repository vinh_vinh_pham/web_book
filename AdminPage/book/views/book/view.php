<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- Small button group -->
    <div class="btn-group">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Xem trước sách <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li>
                <?= Html::a('Xem chi tiết', ['book-view/index', 'id' => $model->id]) ?>
            </li>
            <li>
                <?= Html::a('Thêm xem chi tiết', ['book-view/create', 'id' => $model->id]) ?>
            </li>
        </ul>

    </div>

    <div class="btn-group">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tác giả
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li>
                <?= Html::a('Xem tác giả', ['book-auther/index', 'book_id' => $model->id]) ?>
            </li>
            <li>
                <?= Html::a('Thêm tác giả', ['book-auther/create', 'id' => $model->id]) ?>
            </li>
        </ul>
    </div>

    <div class="btn-group">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Giá sách
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li>
                <?= Html::a('Xem giá sách', ['price/view', 'book_id' => $model->id]) ?>
            </li>
            <li>
                <?= Html::a('Sửa giá sách', ['price/update', 'book_id' => $model->id]) ?>
            </li>
        </ul>
    </div>

    <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
    <?=
    Html::a('Xóa', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm',
        'data' => [
            'confirm' => 'Bạn muốn xóa sách này?',
            'method' => 'post',
        ],
    ])
    ?>
</div>

</p>

<?php
$sql = 'SELECT bt.id as id, concat(pl.name, " - ", tc.name) as name'
        . ' FROM tbl_book_type as bt'
        . ' join tbl_product_list as pl'
        . ' on bt.product_list_id = pl.id'
        . ' join tbl_category as tc'
        . ' on bt.category_id = tc.id'
        . ' where bt.id = ' . $model->type_id;
$connection = \Yii::$app->db;
$condition = $connection->createCommand($sql)->queryAll();
$type = $condition[0]['name'];
?>
<?=
DetailView::widget([
    'model' => $model,
    'options' => ['class' => 'table table-hover table-striped'],
    'attributes' => [
        [
            'attribute' => 'Ảnh',
            'value' => app\models\Images::findOne($model->image_id) ? "http://localhost/book/uploads/" . (app\models\Images::findOne($model->image_id)->name . '.' . app\models\Images::findOne($model->image_id)->ext) : '',
            'format' => ['image', ['width' => '200', 'height' => '284']],
        ],
        'name',
        [
            'label' => 'Tình trạng',
            'value' => \app\models\Condition::findOne($model->condition_id)->name,
        ],
        [
            'label' => 'Nhà xuất bản',
            'value' => $model->manufacture_id ? \app\models\Manufacturer::findOne($model->manufacture_id)->name : 'Không có',
        ],
        [
            'label' => 'Công ty phát hành',
            'value' => $model->issuers_id ? \app\models\Issuers::findOne($model->issuers_id)->name : 'Không có',
        ],
        [
            'label' => 'Hình thức',
            'value' => \app\models\BookForm::findOne($model->condition_id)->name,
        ],
        'weight',
        'page',
        'year_id',
        [
            'label' => 'Năm xuất bản',
            'value' => $model->year_id ? \app\models\ProductionYear::findOne($model->year_id)->year : 'Không có',
        ],
        'width',
        'height',
        [
            'label' => 'Loại sách',
            'value' => $type
        ],
    ],
])
?>

</div>
