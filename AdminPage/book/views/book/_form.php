<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageview').attr('src', e.target.result);
                $('.imageview').show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<style>
    .imageview #imageview{
        width: 200px;
        height: 284px;
    }
</style>
<div class="book-form">

    <?php
    $form = ActiveForm::begin([ 'options' => [
                    'enctype' => 'multipart/form-data'
    ]]);
    ?>

    <div class="form-group">
        <?= yii\helpers\Html::fileInput("image", '', ['class' => "btn btn-info", 'onchange' => "readURL(this);"]) ?>
    </div>

    <?php if ($model->isNewRecord) { ?>
        <div class="form-group imageview" style=" display: none;">
            <img id="imageview" src="#" alt="Ảnh" />
        </div>

    <?php } else { ?>
        <div class="form-group imageview">
            <img id="imageview" src="<?= app\models\Images::findOne($model->image_id) ? "http://localhost/book/uploads/" . (app\models\Images::findOne($model->image_id)->name . '.' . app\models\Images::findOne($model->image_id)->ext) : '' ?>" alt="Ảnh" />
        </div>
    <?php } ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?php
    $condition = \app\models\Condition::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    ?>
    <div class="form-group">
        <?php $action = \Yii::$app->getUrlManager()->createUrl('condition/index'); ?>
        <?= $form->field($model, 'condition_id', ['template' => '<a href="' . $action . '" target="_blank">Tình trạng</a>{input}'])->dropDownList($listData) ?>
    </div>

    <?php
    $condition = \app\models\Manufacturer::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('manufacturer/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'manufacture_id', ['template' => '<a href="' . $action . '" target="_blank">Nhà xuất bản</a>{input}'])->dropDownList($listData) ?>
    </div>

    <?php
    $condition = \app\models\Issuers::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('issuers/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'issuers_id', ['template' => '<a href="' . $action . '" target="_blank">Công ty phát hành</a>{input}'])->dropDownList($listData) ?>
    </div>

    <?php
    $condition = \app\models\BookForm::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('book-form/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'book_form_id', ['template' => '<a href="' . $action . '" target="_blank">Hình thức</a>{input}'])->dropDownList($listData) ?>
    </div>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'page')->textInput() ?>

    <?php
    $condition = \app\models\ProductionYear::find()->all();
    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'year');
    $action = \Yii::$app->getUrlManager()->createUrl('production-year/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'year_id', ['template' => '<a href="' . $action . '" target="_blank">Năm phát hành</a>{input}'])->dropDownList($listData) ?>
    </div>

    <?= $form->field($model, 'width')->textInput() ?>

    <?= $form->field($model, 'height')->textInput() ?>

    <?php
    $sql = 'SELECT bt.id as id, concat(pl.name, " - ", tc.name) as name'
            . ' FROM tbl_book_type as bt'
            . ' join tbl_product_list as pl'
            . ' on bt.product_list_id = pl.id'
            . ' join tbl_category as tc'
            . ' on bt.category_id = tc.id';
    $connection = \Yii::$app->db;
    $condition = $connection->createCommand($sql)->queryAll();

    $listData = yii\helpers\ArrayHelper::map($condition, 'id', 'name');
    $action = \Yii::$app->getUrlManager()->createUrl('book-type/index');
    ?>
    <div class="form-group">
        <?= $form->field($model, 'type_id', ['template' => '<a href="' . $action . '" target="_blank">Loại sách</a>{input}'])->dropDownList($listData) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Sửa', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
