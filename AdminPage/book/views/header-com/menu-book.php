<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\HomeAsset;

/* @var $this \yii\web\View */

$prl = app\models\ProductList::find()->all();
?>
<div class="category">
    <div class="sidebar-box-heading">
        <span class="glyphicon glyphicon-list"></span>       
        <span class="sidebar-box-heading-text">Danh mục sách</span>
    </div>
    <div class="clear"></div>
    <div class="menu-book" style="position:absolute;">    
        <ul>
            <?php
            foreach ($prl as $key => $value1) {
                $cate = app\models\Category::find(['product_list_id' => $value1['id']])
                                ->innerJoin("tbl_book_type as tp", 'tp.category_id = tbl_category.id')
                                ->andWhere(['tp.product_list_id' => $value1['id']])->all();
                ?>
                <li><span><?= $value1['name'] ?></span>
                    <div class="submenu-book">
                        <ul>
                            <?php
                            foreach ($cate as $key => $value) {
                                echo '<li>' . (Html::a($value['name'], ['type', 'pr_id' => $value1['id'], 'cate_id' => $value['id']], ['rel' => 'prl_' . $value1['id'],])) . '</li>';
                            }
                            ?>                        
                        </ul>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>