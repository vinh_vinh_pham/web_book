<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBookAuther */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sách - tác giả';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-auther-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm tác giả', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'STT'],
            'book_id',
            'auther_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
