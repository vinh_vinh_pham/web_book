<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BookAuther */

$this->title = 'Thêm tác giả';
$this->params['breadcrumbs'][] = ['label' => 'Sách - tác giả', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-auther-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
