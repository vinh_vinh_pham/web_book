<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductionYear */

$this->title = 'Thêm năm';
$this->params['breadcrumbs'][] = ['label' => 'Năm phát hành', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="production-year-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
