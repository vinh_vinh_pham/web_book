<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Issuers */

$this->title = 'Sửa thông tin: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Công ty phát hành', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="issuers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
