<style>
    .ctl03_rpt1_img{
        width: 160px;
        height: 160px;
    }
</style>

<div class="contentabc">   
    <div class="right">
        <ul id="crumbs">
            <li><a href="hanhtrangvaodaihoc.html">
                    <img alt="Trở về trang chủ" src="http://localhost/book/images/home.png" /></a></li>
            <li><a href="lop-10-c41.aspx.html"><?= app\models\ProductList::findOne($pr_id)->name ?></a>
        </ul>
        <div class="clear">
        </div>
        <div class="title-pcat">
            <div class="f">
                &nbsp;</div>
            <h1><?= $cate_id ? app\models\Category::findOne($cate_id)->name : "Tất cả sách" ?></h1>
        </div>
        <div class="clear">
        </div>

        <div class="pcat">
            <ul>

                <?php
                if (!$cate_id) {
                    $book = app\models\Book::find()
                                    ->innerJoin("tbl_book_type as tp", 'tbl_book.type_id = tp.id')
                                    ->andWhere(['tp.product_list_id' => $pr_id])->all();
                } else {
                    $book = app\models\Book::find()
                                    ->innerJoin("tbl_book_type as tp", 'tbl_book.type_id = tp.id')
                                    ->andWhere(['tp.product_list_id' => $pr_id, 'tp.category_id' => $cate_id])->all();
                }
                foreach ($book as $key => $value) {
                    ?>

                    <li>
                        <a id="ctl03_rpt1_hplImg_0" href="#">
                            <img class="ctl03_rpt1_img" src="<?= app\models\Images::findOne($value['image_id']) ? "http://localhost/book/uploads/" . (app\models\Images::findOne($value['image_id'])->name . '.' . app\models\Images::findOne($value['image_id'])->ext) : '' ?>" />
                        </a>
                        <div class="model-name">
                            <?= \yii\helpers\Html::a($value['name']) ?>
                        </div>
                        <div id="ctl03_rpt1_price_old_0" class="price-old">
                            450.000 VNĐ
                        </div>
                        <div class="price">
                            428.000 VNĐ

                            <div id="ctl03_rpt1_saleoff_0" class="dis-price">
                                -5%
                            </div>
                        </div>
                    </li>

                <?php }
                ?>

            </ul>
        </div>

        <div class="clear">
        </div>
        <div class="clear">
        </div>
        <div id="ContentPlaceHolder1_ProductListControl1_pnlPaging" class="paging">



        </div>

    </div>

</div>