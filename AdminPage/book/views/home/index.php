<div class="contentabc">

    <style>
        .Slides_Container img{
            width: 540px;
            height: 240px;
        }
        .ctl03_rpt1_img{
            width: 130px;
            height: 160px;
        }
    </style>

    <div  width="540" height="280">

    </div>

    <div class="right-home">
        <iframe scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:220px; height:278px; margin-left:-3px" allowTransparency="true"></iframe>

    </div>
    <div class="clear">
    </div>

    <div class="newletter">

        <input name="HomeNewsLetterControl1$txtNewLetter" type="text" value="Địa chỉ email của bạn" id="HomeNewsLetterControl1_txtNewLetter" class="txtNewletter" />
        <input type="submit" name="HomeNewsLetterControl1$btnNewletter" value="ĐĂNG KÝ" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions( & quot; HomeNewsLetterControl1$btnNewletter & quot; , & quot; & quot; , true, & quot; 1 & quot; , & quot; & quot; , false, false))" id="HomeNewsLetterControl1_btnNewletter" />
        <span id="HomeNewsLetterControl1_RequiredFieldValidator1" style="display:none;"></span>
        <span id="HomeNewsLetterControl1_RegularExpressionValidator1" style="display:none;"></span>
        <div id="HomeNewsLetterControl1_ValidationSummary1" style="display:none;">

        </div>

    </div>

    <div class="home-middle">
        <div style="float:left; margin-right:10px">
        </div>
    </div>

    <div class="clear">
    </div>

    <div class="clear">
    </div>

    <div class="clear">
    </div>
    <?php $url = \Yii::$app->getUrlManager()->createUrl(['home/view', 'book_id' => 1]); ?>
    <div id="HomeBestSellerControl1_pnlBanChay" class="box-home" style="border-left: none; border-right: none; width: 998px">
        <div class="title">
            <h3>Sản phẩm bán chạy</h3>
        </div>
        <div class="clear">
        </div>
        <div>
            <ul class="mycarousel jcarousel-skin-tango">

                <?php for ($i = 0; $i < 5; $i++) { ?>
                    <li>
                        <a id="HomeBestSellerControl1_rpt1_hplImg_0" href="<?= $url ?>">
                            <img class="ctl03_rpt1_img" src="http://localhost/book/uploads/20150705122843.jpg" />
                        </a>
                        <div class="model-name">
                            <a id="HomeBestSellerControl1_rpt1_hplTitle_0" href="<?= $url ?>">Những Viên Kim Cương Trong Hoá Học</a>
                        </div>
                        <div id="HomeBestSellerControl1_rpt1_price_old_0" class="price-old">
                            450.000 VNĐ
                        </div>
                        <div class="price">
                            428.000 VNĐ
                        </div>
                    </li>
                <?php } ?>

            </ul>
        </div>
    </div>

    <?php
    $prList = app\models\ProductList::find()->all();
    foreach ($prList as $key => $value) {
        ?>
        <div class="box-home">
            <div class="title">
                <h3><?= $value['name'] ?></h3>
                <div class="sub">
                    <?php
                    $cate = app\models\Category::find(['product_list_id' => $value['id']])
                                    ->innerJoin("tbl_book_type as tp", 'tp.category_id = tbl_category.id')
                                    ->andWhere(['tp.product_list_id' => $value['id']])->all();
                    foreach ($cate as $key => $value1) {
                        echo \yii\helpers\Html::a($value1['name'], ['type', 'pr_id' => $value['id'], 'cate_id' => $value1['id']]) . ' | ';
                    }
                    ?>
                </div>
            </div>

            <div class="clear">
            </div>
            <div id="ctl03_pnlRecord">
                <ul class="mycarousel jcarousel-skin-tango">

                    <?php
                    $book = app\models\Book::find()
                                    ->innerJoin("tbl_book_type as tp", 'tbl_book.type_id = tp.id')
                                    ->andWhere(['tp.product_list_id' => $value['id']])->all();
                    foreach ($book as $key => $value1) {
                        ?>

                        <li>
                            <a id="ctl03_rpt1_hplImg_0" href="<?= $url ?>">
                                <img class="ctl03_rpt1_img" src="<?= app\models\Images::findOne($value1['image_id']) ? "http://localhost/book/uploads/" . (app\models\Images::findOne($value1['image_id'])->name . '.' . app\models\Images::findOne($value1['image_id'])->ext) : '' ?>" />
                            </a>
                            <div class="model-name">
                                <?= \yii\helpers\Html::a($value1['name'], ['view', 'book_id' => $value1['id']]) ?>
                            </div>
                            <div id="ctl03_rpt1_price_old_0" class="price-old">
                                450.000 VNĐ
                            </div>
                            <div class="price">
                                428.000 VNĐ
                            </div>
                        </li>

                    <?php }
                    ?>

                </ul>
            </div>
        </div>


    <?php } ?>

</div>