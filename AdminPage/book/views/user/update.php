<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Sửa tài khoản';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title . ': ' . $model->username) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
