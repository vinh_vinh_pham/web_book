<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Price */

$this->title = 'Sửa giá:' . ' ' . (\app\models\Book::findOne($model->book_id)->name);
$this->params['breadcrumbs'][] = ['label' => 'Giá sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="price-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
