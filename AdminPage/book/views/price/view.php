<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Price */

$this->title = "Sách: " . (\app\models\Book::findOne($model->book_id)->name);
$this->params['breadcrumbs'][] = ['label' => 'Giá sách', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-hover table-striped'],
        'attributes' => [
            'sell',
            'market',
        ],
    ])
    ?>

    <p>
        <?= Html::a('Sửa', ['update', 'book_id' => $model->book_id], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
