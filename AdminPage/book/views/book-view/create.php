<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BookView */

$this->title = 'Thêm xem chi tiết';
$this->params['breadcrumbs'][] = ['label' => 'Sách', 'url' => ['book/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-view-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
