<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BookView */
/* @var $form yii\widgets\ActiveForm */
?>

<script type="text/javascript">
    function readURL(input) {
        console.log(input.files);
        for (var i = 0; i < input.files.length; i++) {
            if (input.files && input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.listimage').prepend('<img id="imageview" src="#" />');
                    $('#imageview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    }
</script>

<style>
    #imageview{
        padding: 5px;
        width: 20%;
        height: 20%;
    }
</style>

<div class="book-view-form">

    <?php
    $form = ActiveForm::begin([ 'options' => [
                    'enctype' => 'multipart/form-data'
    ]]);
    ?>
    <div class="form-group">
        <?= yii\helpers\Html::fileInput("image[]", '', ['class' => "btn btn-info", 'onchange' => "readURL(this);", 'multiple' => true]) ?>
    </div>

    <div class="form-group listimage">
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Lưu', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
