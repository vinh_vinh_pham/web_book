<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Đăng nhập';
?>
<style>
    body {
        padding-top: 40px;
        background-color: #eee;
    }
    .form-signin {
        max-width: 330px;
        margin: 0 auto;
    }
</style>
<div class="site-login">

    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-signin'],
    ]);
    ?>
    <h2 class="form-signin-heading"><?= Html::encode($this->title) ?></h2>
    <?=
    $form->field($model, 'username', ['template' => '{input}{error}'])->textInput([
        'placeholder' => $model->attributeLabels()['username']])
    ?>

    <?=
    $form->field($model, 'password', ['template' => '{input}{error}'])->passwordInput([
        'placeholder' => $model->attributeLabels()['password']])
    ?>
    <div class="checkbox">
        <?=
        $form->field($model, 'rememberMe', [
            'template' => "{input}{error}",
        ])->checkbox()
        ?>
    </div>
    <div class="form-group">
<?= Html::submitButton('Đăng nhập', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
    </div>
    <div>
    <?= Html::a('Thêm tài khoản', ['user/create']); ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
