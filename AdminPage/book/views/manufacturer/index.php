<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchManufacturer */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nhà sản xuất';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturer-index">
    <p>
        <?= Html::a('Thêm nhà sản xuất', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'STT'],

            'name',
            'address',
            'phone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
