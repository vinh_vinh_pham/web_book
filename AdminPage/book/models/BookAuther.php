<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_book_auther".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $auther_id
 */
class BookAuther extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_book_auther';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'auther_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'auther_id' => 'Auther ID',
        ];
    }
}
