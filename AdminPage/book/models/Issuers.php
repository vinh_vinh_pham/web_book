<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_issuers".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $phone
 */
class Issuers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_issuers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'address' => 'Địa chỉ',
            'phone' => 'Điện thoại',
        ];
    }
}
